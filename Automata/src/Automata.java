public class Automata {

    int cont;
    int iterator;
    int item;
    boolean aceptado;
    char[] car;
    char[] c;
    char[] d;
    char[] t;

    public static void main(String[] args) {
        Automata aut = new Automata();
        String cadena = "18bl4534422@unedl.mx";
        aut.c = new char[]{'b', 'l','a','l'};
        aut.d = new char[]{'u','n','e','d','l'};
        aut.t = new char[]{'m','x'};
        aut.car = cadena.toCharArray();
        aut.inicio();
        System.out.println("");

    }

    public void inicio() {
        cont = 0;
        iterator = 0;
        item = 0;
        aceptado = false;
        q0();
    }

    public void q0() {
        if(cont<car.length){

            if(car[cont] >= 48 && car[cont]<=59){
                cont++;
                iterator++;
                if(iterator==2){
                    iterator = 0;
                    q1();

                }else{
                    q0();
                }
            }else{
                qError();
            }
        }
    }

    public void q1() {

        if(cont<car.length){
            if(item<c.length-2){
                if(car[cont] == c[item] || car[cont] == c[item+2]){
                    item++;
                    cont++;
                    iterator++;
                    q1();
                }else{
                    qError();
                }
            }else{
                iterator = 0;
                item = 0;
                q2();
            }
        }else{
            qError();
        }
    }

    public void q2() {
        if(cont<car.length){
            if(iterator <7) {
                if (car[cont] >= 48 && car[cont] <= 59) {
                    cont++;
                    iterator++;
                    q2();
                } else {
                    if(car[cont] == 64 && iterator > 5){
                        cont++;
                        q3();
                    }else {
                        qError();
                    }
                }
            }else{
                if(car[cont] == 64 && iterator > 5){
                    cont++;
                    q3();
                }else {
                    qError();
                }
            }
        }else{
            qError();
        }
    }

    public void q3(){
        if(cont<car.length){
            if(item<d.length){
                if(car[cont] == d[item]){
                    item++;
                    cont++;
                    iterator++;
                    q3();
                }else{
                    qError();
                }
            }else{
                iterator = 0;
                item = 0;
                if(car[cont] == 46) {
                    cont++;
                    q4();
                }else{
                    qError();
                }
            }
        }else{
            qError();
        }
    }

    public void q4(){
        if(cont<car.length){
            if(item<t.length){
                if(car[cont] == t[item]){
                    item++;
                    cont++;
                    iterator++;

                    q4();
                }else{
                    qError();
                }
            }else{
                if(car[cont] >= 32 && car[cont] <= 255){
                    qError();
                }else{
                    q5();
                }

            }
        }else{
            q5();
        }
    }
    public void q5(){
        System.out.println("lo lograste");
    }



    public void qError(){
        System.out.println("no coincide la cadena");
    }

}
