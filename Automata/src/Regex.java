import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex{

    public static void main(String[] args) {
        Pattern pat = Pattern.compile("^([0-9]{2})([bl | al]{2})([0-9]{6,7})@([unedl]*)\\.([mx]+)$");
        String input = "18bl1111111@unedl.mx";
        Matcher mat = pat.matcher(input);
        if (mat.find()) {
            System.out.println("regex encontrada");
        } else {
            System.err.println("regex NO encontrada");
        }
    }
}
